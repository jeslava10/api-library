let server = require("../app");
let chai = require("chai");
let chaiHttp = require("chai-http");

// Assertion 
chai.should();
chai.use(chaiHttp); 

describe('Api Library', () => {

    /**
     * Test Get All books
     */
    describe('Test GET route', () => {
        it('listado de libros',(done) => {
            chai.request(server)
                .get("/app/books")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                done();
            });
        });

        it('error en caso de no encontrar lregistros',(done) => {
            chai.request(server)
                .get("/app/books")
                .end((err, response) => {
                    response.should.have.status(404);
            done();
            });
        });
    });

    /**
     * Test Get books by id
     */
    describe('Test GET route /app/books/:id',()=>{
        it('Buscar el libro por el id',(done)=>{
            const bookId = 1;
            chai.request(server)
                .get("/app/books" + bookId)
                .end((err,response)=>{
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id');
                    response.body.should.have.property('isbn');
                    response.body.should.have.property('title');
                    response.body.should.have.property('author');
                    response.body.should.have.property('description');
                    response.body.should.have.property('publication_date');
                    response.body.should.have.property('language');
                    response.body.should.have.property('pages');
                    response.body.should.have.property('price'); 
                    response.body.should.have.property('id').eq(1);
                done();
            });
        });

        it("Error en caso de no encontrar registros", (done) => {
            const bookId = 123;
            chai.request(server)                
                .get("/app/books" + bookId)
                .end((err, response) => {
                    response.should.have.status(404);
                    response.text.should.be.eq("El libro con Id proporcionado no existe.");
                done();
            });
        });
    });

    /**
     * Test POST Book
     */
    describe('Test route POST /app/books',() => {
        it('Registro de libros', (done) => {
            const book = {
                //aqui el objeto book
                id: 1,
                isbn: '001',
                title: '100 Años de soledad',
                author: 'Gabriel Garcia Marquez',
                description: 'Prueba descripcion',
                publication_date: '24/05/2008',
                language: 'Español',
                pages: 100,
                price: 5000
            };
            chai.request(server)
                .post("/app/books/")
                .send(book)
                .end((err, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id');
                    response.body.should.have.property('isbn');
                    response.body.should.have.property('title');
                    response.body.should.have.property('author');
                    response.body.should.have.property('description');
                    response.body.should.have.property('publication_date');
                    response.body.should.have.property('language');
                    response.body.should.have.property('pages');
                    response.body.should.have.property('price'); 
                    response.body.should.have.property('message').eq('El Libro fue agregado exitosamente')

                done();
            });
        });

        it("Error en caso de fallar en envio de los datos", (done) => {
            const book = {
                //aqui el objeto
                id: 1
            };
            chai.request(server)                
                .post("/app/books/")
                .send(book)
                .end((err, response) => {
                    response.should.have.status(400);
                    response.text.should.be.eq("Todas las propiedades son requeridas");
                done();
            });
        });
    });

    /**
     * Test PUT Book
     */
    describe("Test PUT route /app/books/:id", () => {
        it("Test al modificar un libro", (done) => {
            const bookId = 1;
            const book = {
                //Objeto aqui
                id: 1,
                isbn: '001',
                title: '100 Años de soledad',
                author: 'Gabriel Garcia Marquez',
                description: 'Prueba actualiza descripcion',
                publication_date: '24/05/2008',
                language: 'Español',
                pages: 100,
                price: 5000
            };
            chai.request(server)                
                .put("/app/books/" + bookId)
                .send(book)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id');
                    response.body.should.have.property('isbn');
                    response.body.should.have.property('title');
                    response.body.should.have.property('author');
                    response.body.should.have.property('description');
                    response.body.should.have.property('publication_date');
                    response.body.should.have.property('language');
                    response.body.should.have.property('pages');
                    response.body.should.have.property('price'); 
                    response.body.should.have.property('id').eq(1);
                done();
            });
        });

        it("Error en la actualizacion del libro", (done) => {
            const bookId = 1;
            const book = {
                //Objeto aqui
                id: 1,
                isbn: '001',
                title: '100 Años de soledad'
            };
            chai.request(server)                
                .put("/app/books/" + bookId)
                .send(task)
                .end((err, response) => {
                    response.should.have.status(400);
                    response.text.should.be.eq("Se presentaron problemas al realizar la actualizacion");
                done();
            });
        });        
    });

    /**
     * Test DELETE Book
     */
    describe("Test DELETE route /api/book/:id", () => {
        it("Test al eliminar un registro", (done) => {
            const bookId = 1;
            chai.request(server)                
                .delete("/api/books/" + bookId)
                .end((err, response) => {
                    response.should.have.status(200);
                done();
            });
        });

        it("Error el eliminar un registro del libro", (done) => {
            const bookId = 145;
            chai.request(server)                
                .delete("/api/books/" + bookId)
                .end((err, response) => {
                    response.should.have.status(404);
                    response.text.should.be.eq("El Libro cn el Id proporcionado no existe.");
                done();
            });
        });
    });

});