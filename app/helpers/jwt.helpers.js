const jdt = require('jsonwebtoken');
const env = require('../../env.js');

const generarJWT = ( uid ) => {
    return new Promise( (resolve, reject) => {
        
        const payload = { uid };
        
        jdt.sign( payload, env.SECRETKEY, {
            expiresIn: '4h'
        }, (err, token ) => {
            if ( err ) {
                console.log( error );
                reject( 'TOKEN_NOT_BE_GENERATE' );
            } else {
                resolve( token );
            }
        });
    });
}

module.exports = {
    generarJWT
}