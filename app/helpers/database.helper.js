const Book = require('../models/book.model')

/**
 * @name isBook
 * @description Helper encargado de validar si el id de un libro existee.
 * @param {number} id identificador del libro 
 */
const isBook = async (id) => {
    const isBook = await Book.findByPk( id );

    if ( !isBook ) {
        throw new Error( `THE BOOK WHIT ID ${ id } NOT EXIST` );
    }
}


module.exports = {
    isBook
}