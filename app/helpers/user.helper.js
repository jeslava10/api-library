const User = require("../models/user.model");

/**
 * @name findUserbyEmailError
 * @description metodo encargado de buscar un usuario por correo, 
 *              en caso de encontrarse retornará error.
 * @param {string} email email de registro.
 */
const findUserbyEmailError = async (email) => {    
    
    const isUser = await User.findOne( { 
        where: {
            email
        } 
    });

    if ( isUser ) {
        throw new Error( `EMAIL_IN_USE` );
    }
}

/**
 * @name findUserbyEmail
 * @description metodo encargado de buscar un usuario por correo, 
 *              en caso de no encontrarse retornará error
 * @param {string} email email de registro.
 */
const findUserbyEmail = async (email) => {    
    
    const isUser = await User.findOne( { 
        where: {
            email
        } 
    });

    if ( !isUser ) {
        throw new Error( `EMAIL_NOT_FOUND` );
    }
}

module.exports = {
    findUserbyEmail,
    findUserbyEmailError
};