const { responseFailure } = require('../controllers/common.controller');

/**
 * @name isAdmin
 * @description Metodo encargado de validar si una ruta deberá ser 
 *              ejecutada unicamente por un administrador.
 * @param {request} req request http
 * @param {response} res response http
 * @param {function} next callback next middleware
 * @returns 
 */
const isAdmin = (req, res, next) => {
    
    const user = req.userTemp;
    console.log('usuario de la petición en isAdmin----->', user, );
    if ( !(user.role.name === 'ADMIN') ) {
        return responseFailure(res, {code: 401, message: 'UNAUTHORIZED', error: 'UNAUTHORIZED'} );
    }

    next();
}

module.exports = {
    isAdmin
}