const { request, response } = require('express');
const jwt = require('jsonwebtoken');
const { responseFailure } = require('../controllers/common.controller');
const Role = require('../models/role.model');
const User = require('../models/user.model');
const env = require('../../env.js');


/**
 * @name validarJWT
 * @description Middleware encargado de validar si el jwt enviado por los headers
 *              es correcto.
 * @param {request} req 
 * @param {response} response 
 * @param {callback} next 
 */
const validarJWT = async (req = request, res= response, next) => {

    const token = req.header('token');
    if ( !token ) {
        return responseFailure(res, { code: 401, message: 'TOKEN_NOT_FOUND'} );
    }

    try {
        const { uid } = jwt.verify( token, env.SECRETKEY);
        const user = await User.findOne( { 
            where: {id : uid}, 
            include: Role,
            attributes: { exclude: ['password', 'creationDate', 'roles_id'] }
        });

        if ( !user ) {
            return responseFailure(res, { code: 401, message: 'USER_NOT_FOUND'} );
        }
        req.userTemp = user.toJSON();

        next();
    } catch (error) {
        return responseFailure(res, { code: 401, message: 'INVALID_TOKEN'}, error );
    }
}



module.exports = {
    validarJWT
}