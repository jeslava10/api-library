const { validationResult } = require('express-validator');
const { responseFailure } = require('../controllers/common.controller');


const middlewareValidate = (req, res, next) => {
    
    const errors = validationResult( req );
    
    if ( !errors.isEmpty() ) {
        return responseFailure(res, {error: errors.errors} );
    }

    next();
}

module.exports = {
    middlewareValidate
}