const OrderDetail = require('./order-detail.model');
const Order = require('./order.model');
const Role = require('./role.model');
const User = require('./user.model');
const Book = require('./book.model');



/**
 * Relación uno a muchos de usuarios y roles
 */
Role.hasMany(User , {
    foreignKey: 'roles_id'
} );

User.belongsTo(Role, {
    foreignKey: 'roles_id'
});


/**
 * Relación uno a muchos de usuarios y ordenes
 */
User.hasMany(Order , {
    foreignKey: 'users_id'
} );

Order.belongsTo(User, {
    foreignKey: 'users_id'
});


/**
 * Relación uno a muchos de detalle de orden y orden
 */
Order.hasMany(OrderDetail , {
    foreignKey: 'orders_id'
} );

OrderDetail.belongsTo(Order, {
    foreignKey: 'orders_id'
});


/**
 * Relación uno a muchos de detalle de orden y libros
 */
Book.hasMany(OrderDetail , {
    foreignKey: 'books_id'
} );

OrderDetail.belongsTo(Book, {
    foreignKey: 'books_id'
});