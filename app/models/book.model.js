const { DataTypes } = require('sequelize');
const database = require('../database/database');


/**
 * @name Book
 * @description Modelo encargado de representar a 
 *              la entidad que almacenará la información 
 *              de los libros disponiblees en la aplicación
 */
const Book = database.define('book', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        allowNull: false
    },
    isbn: {
        type: DataTypes.STRING,
        unique: true,
        field: 'isbn',
        allowNull: false
    },
    title: {
        type: DataTypes.STRING,
        field: 'title',
        allowNull: false
    },
    author: {
        type: DataTypes.STRING,
        field: 'author',
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        field: 'description',
        allowNull: false
    },
    publicationDate: {
        type: DataTypes.DATE,
        field: 'publication_date',
        allowNull: false
    },
    language: {
        type: DataTypes.STRING,
        field: 'language',
        allowNull: false
    },
    pages: {
        type: DataTypes.INTEGER,
        field: 'pages',
        allowNull: false
    },
    price: {
        type: DataTypes.INTEGER,
        field: 'price',
        allowNull: false
    }
}, {
    tableName: 'books',
    timestamps: false
});


module.exports = Book;