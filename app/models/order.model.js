const { DataTypes } = require('sequelize');
const database = require('../database/database');
const User = require('./user.model');


/**
 * @name Order
 * @description Modelo encargado de representar a 
 *              la entidad que almacenará la información 
 *              de las ordenes
 */
const Order = database.define('order', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        allowNull: false
    },
    orderDate: {
        type: DataTypes.DATE,
        field: 'order_date',
        defaultValue: DataTypes.NOW,
        allowNull: false
    },
    totalPrice: {
        type: DataTypes.INTEGER,
        field: 'total_price',
        allowNull: false
    },
    totalQuantity: {
        type: DataTypes.INTEGER,
        field: 'total_quantity',
        allowNull: false
    }
}, {
    tableName: 'orders',
    timestamps: false
});



module.exports = Order;