
const cors = require('cors');
const express = require('express');
const swaggerUI = require('swagger-ui-express');
const database = require('../database/database');
const swaggerDocs = require('../swagger/swagger');
const env = require('../../env.js');

class Server {
    
    app;
    port;

    constructor() {
        this.app = express();
        this.port = env.PORT || 3000;
        
        this.relations();

        //Connect db
        this.connectDb();

        //Middlewares
        this.middlewares();

        //Rutas
        this.routes();
    }


    relations() {
        require('./relations');
    }

    async connectDb() {
        try {
            await database.authenticate();
            console.log('Conexión exitosa');
        } catch (error) {
            throw new Error( error );  
        }
    }

    middlewares() {
        this.app.use( cors() );
        this.app.use( express.json() );
        
        this.app.use( '/api-docs', swaggerUI.serve, swaggerUI.setup( require('../swagger/swagger.json') ) );
    }
    
    routes() {
        this.app.use('/app/book', require('../routes/books.routes'));
        this.app.use('/app/auth', require('../routes/auth.routes'));
        this.app.use('/app/order', require('../routes/order.routes'));
    }

    listen() {
        this.app.listen( this.port, "0.0.0.0" , ()=> {
            console.log('server run : ', this.port);
        })
    }

}

module.exports = Server;