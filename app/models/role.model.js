const { DataTypes } = require('sequelize');
const database = require('../database/database');
const User = require('./user.model');


/**
 * @name Role
 * @description Modelo encargado de representar a 
 *              la entidad que almacenará la información 
 *              de los roles en la aplicación
 */
const Role = database.define('role', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        field: 'name',
        allowNull: false,
        unique: true
    }
}, {
    tableName: 'roles',
    timestamps: false
});

module.exports = Role;