const { DataTypes } = require('sequelize');
const database = require('../database/database');
const Book = require('./book.model');
const Order = require('./order.model');


/**
 * @name OrderDetail
 * @description Modelo encargado de representar a 
 *              la entidad que almacenará la información 
 *              de los detalles de las ordenes
 */
const OrderDetail = database.define('order_details', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        allowNull: false
    },
    totalDetail: {
        type: DataTypes.INTEGER,
        field: 'total_detail',
        allowNull: false
    },
    quantity: {
        type: DataTypes.INTEGER,
        field: 'quantity',
        allowNull: false
    },
    detail_value: {
        type: DataTypes.INTEGER,
        field: 'detail_value',
        allowNull: false
    }
}, {
    tableName: 'order_details',
    timestamps: false
});


module.exports = OrderDetail;