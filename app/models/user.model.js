const { DataTypes } = require('sequelize');
const database = require('../database/database');
const Role = require('./role.model');


/**
 * @name User
 * @description Modelo encargado de representar a 
 *              la entidad que almacenará la información 
 *              de los usuarios en la aplicación
 */
const User = database.define('user', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        allowNull: false
    },
    firstName: {
        type: DataTypes.STRING,
        field: 'first_name',
        allowNull: false
    },
    secondName: {
        type: DataTypes.STRING,
        field: 'second_name',
    },
    lastName: {
        type: DataTypes.STRING,
        field: 'last_name',
        allowNull: false
    },
    secondLastName: {
        type: DataTypes.STRING,
        field: 'second_last_name'
    },
    email: {
        type: DataTypes.STRING,
        field: 'email',
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        field: 'password',
        allowNull: false
    },
    creationDate: {
        type: DataTypes.DATE,
        field: 'creation_date',
        defaultValue: DataTypes.NOW,
        allowNull: false
    }
}, {
    tableName: 'users',
    timestamps: false
});


module.exports = User;