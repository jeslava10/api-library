const Sequelize = require('sequelize');
const env = require('../../env.js');

const database = new Sequelize(env.DATABASE, env.USERNAME, env.PASSWORD, {
  host: env.HOST,
  dialect: env.DIALECT,
  // logging: (...msg) => console.log(msg),
  pool: env.POOL
});

module.exports = database;