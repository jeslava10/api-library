CREATE TABLE IF NOT EXISTS `books` (
  `id` INT NOT NULL,
  `isbn` VARCHAR(45) NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `publicationDate` DATE NULL,
  `language` VARCHAR(45) NOT NULL,
  `pages` INT NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `isbn_UNIQUE` UNIQUE (`isbn` ASC) );
  
  CREATE TABLE IF NOT EXISTS `roles` (
  `id` INT NOT NULL COMMENT 'Identificador del rol',
  `name` VARCHAR(20) NOT NULL COMMENT 'Nombre unico del rol',
  PRIMARY KEY (`id`),
  CONSTRAINT `name_UNIQUE` UNIQUE (`name` ASC) );
  
  CREATE TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador del usuario',
  `first_name` VARCHAR(15) NOT NULL COMMENT 'Primer nombre del usuario; requerido',
  `second_name` VARCHAR(15) NULL COMMENT 'Segundo nombre del usuario',
  `last_name` VARCHAR(15) NOT NULL COMMENT 'Primer apellido del usuario; requerido',
  `second_last_name` VARCHAR(15) NULL COMMENT 'Segundo apellido del usuario;',
  `email` VARCHAR(45) NOT NULL COMMENT 'Dirección de correo electronico del usuario.',
  `password` VARCHAR(45) NOT NULL COMMENT 'constraseña del usuario; encriptada',
  `roles_id` INT NOT NULL COMMENT 'foranea del rol asociado al usuario',
  PRIMARY KEY (`id`),
  CONSTRAINT `email_UNIQUE` UNIQUE (`email` ASC) ,
  INDEX `fk_users_roles_idx` (`roles_id` ASC) ,
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_id`)
    REFERENCES `roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
   CREATE TABLE IF NOT EXISTS `orders` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Idenetificador de la orden',
  `date_order` DATETIME NOT NULL COMMENT 'Fecha de la orden',
  `total_price` DECIMAL(10) NOT NULL COMMENT 'Precio total de la orden',
  `users_id` INT NOT NULL COMMENT 'Foreanea que representa el usuario asociado a la orden',
  PRIMARY KEY (`id`),
  INDEX `fk_orders_users1_idx` (`users_id` ASC) ,
  CONSTRAINT `fk_orders_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'identificador del detalle de la orden',
  `orders_id` INT NOT NULL COMMENT 'Identificador del id de la orden principal',
  `books_id` INT NOT NULL COMMENT 'Identificador del producto',
  PRIMARY KEY (`id`),
  INDEX `fk_order_details_orders1_idx` (`orders_id` ASC) ,
  INDEX `fk_order_details_books1_idx` (`books_id` ASC) ,
  CONSTRAINT `fk_order_details_orders1`
    FOREIGN KEY (`orders_id`)
    REFERENCES `orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_details_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);    