/**
 * @name responseSuccess
 * @description Maneja la respuesta correcta a retornar por el controlador.
 * @param {response} response Respuesta esperada por la petición http
 * @param {*} params indica los parametros para gestionar la respuesta http 
 * @returns respuesta http a rerotnar por el controlador
 */
 const responseSuccess = (
    response, 
    params = { message: 'SUCCESS', code: 200, resultStatus: 'OK', response: null }) => {
    return response.json({
        ...paramsSuccess, 
        ...params
    });
}

/**
 * @name responseFailure
 * @description Maneja la respuesta erronea a retornar por el controlador.
 * @param { response } response Respuesta esperada por la petición http
 * @param { Object } params indica los parametros para gestionar la respuesta http
 * @param { throw } error Error capturado por el try cath indicando el fallo.
 * @returns respuesta http a rerotnar por el controlador
 */
const responseFailure = ( 
    response, 
    params = { message: 'FAIL', code: 400, resultStatus: 'FAILURE', response: null, error: null }, 
    error = null) => {
    
    if ( error ) {
        console.error('ResponseFailure--------->', error);
    } 
    return response.status( params.code || paramsFailure.code  ).json({
        ...paramsFailure, 
        ...params
    });
}

/**
 * @name paramSuccess
 * @description Definición de respuesta generica exitosa
 */
const paramsSuccess = { 
    message: 'SUCCESS', 
    code: 200, 
    resultStatus: 'OK', 
    response: null
}

/**
 * @name paramsFailure
 * @description Definición de respuesta generica exitosa
 */
const paramsFailure = { 
    message: 'FAIL', 
    code: 400, 
    resultStatus: 'FAILURE', 
    response: null,
    error: null
}



module.exports = {
    responseSuccess,
    responseFailure
}