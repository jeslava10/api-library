const { request, response } = require("express");
const { Op } = require("sequelize");
const Book = require("../models/book.model");
const OrderDetail = require("../models/order-detail.model");
const Order = require("../models/order.model");

const { responseSuccess, responseFailure } = require("./common.controller");


/**
 * @name getAllOrders
 * @description Metodo encargado de obtener todos los libros.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const getAllOrders = async ( req = request, res = response ) => {
    
    const userTemp = req.userTemp;
    try {
        console.log('Usuario temporal', userTemp.role);
        const orders = await Order.findAll({include: OrderDetail});
        responseSuccess(res, {message: 'GET_ORDERS', response: orders});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_GET_ORDERS'}, error);
    }
}


/**
 * @name getOrderByUser
 * @description Metodo encargado de obtener todas las ordenes por usuario.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const getOrderByUser = async ( req = request, res = response ) => {
    try {
        const userTemp = req.userTemp;
        const orders = await Order.findAll( { where: { users_id: userTemp.id}, include: OrderDetail} );
        responseSuccess(res, {message: 'GET_ORDERS_BY_USER', response: orders});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_GET_ORDERS_BY_USER'}, error);
    }
}


/**
 * @name saveOrder
 * @description Metodo post encargado de guardar una orden.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const saveOrder = async ( req = request, res = response ) => {
    
    const { order } = req.body;
    const userTemp = req.userTemp;

    console.log(order);
    
    try {

        /**
         * Se obtiene los identificadore de los libros y la cantidad total de items
         */
        let booksAndQuantity = {
            booksId: [],
            quantity: 0
        };

        order.forEach( orderItem => {
            booksAndQuantity.booksId.push( orderItem.books_id );
            booksAndQuantity.quantity += orderItem.quantity;
        });

        /**
         * Se consultan los libros para obtener la información detallada por articulo
         */

        const booksOrder = await Book.findAll({ 
            where: {
                id: {
                    [Op.in]: booksAndQuantity.booksId
                }
            }
        });

        /**
         * Se procede a armar los detalless de la orden
         */

        let indexBook;
        let quantity = 0;
        let totalOrder = 0;
        const orderDetails = [];

        booksOrder.forEach( book => {

            indexBook = order.findIndex( order => order.books_id === book.id );

            quantity = order[ indexBook ].quantity;
            totalOrder += book.price * quantity;
            orderDetails.push({
                totalDetail: book.price * quantity,
                quantity: quantity,
                detail_value: book.price,
                books_id: book.id
            });
        })

        /**
         * Se procede a persitir la orden
         */        
        const newOrder = await Order.create({
            totalPrice: totalOrder,
            totalQuantity: booksAndQuantity.quantity,
            users_id: userTemp.id, 
            order_details: orderDetails
        }, { include: OrderDetail });
        
        responseSuccess(res, {message: 'SAVE_ORDER', response: newOrder});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_SAVE_ORDER'}, error);
    }
}


/**
 * @name updateOrder
 * @description Metodo encargado de actualizar un libro
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const updateOrder = async ( req = request, res = response ) => {
    const { id } = req.params;
    const body = req.body;
    try {
        const order = new Order({...body, id});
        await order.update();
        responseSuccess(res, {message: 'UPDATE_ORDER', response: order});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_UPDATE_ORDER'}, error);
    }
}


/**
 * @name deleteOrder
 * @description Metodo encargado de eliminar un libro
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const deleteOrder = async( req = request, res = response ) => {
    
    const { id } = req.params;
    try {
        const order = await Order.findByPk( id );

        await order.destroy()
        responseSuccess(res, {message: 'DELETE_ORDER', response: order});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_DELETE_ORDER'}, error);
    }
}


module.exports = {
    getAllOrders,
    getOrderByUser,
    saveOrder,
    updateOrder,
    deleteOrder
}