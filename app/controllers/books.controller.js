const { request, response } = require("express");

const Book = require('../models/book.model');
const { responseSuccess, responseFailure } = require("./common.controller");


/**
 * @name getAllBooks
 * @description Metodo encargado de obtener todos los libros.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const getAllBooks = async ( req = request, res = response ) => {
    try {
        const books = await Book.findAll();
        responseSuccess(res, {message: 'GET_BOOKS', response: books});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_GET_BOOKS'}, error);
    }
}


/**
 * @name getBook
 * @description Metodo encargado de obtener todos los libros.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const getBook = async ( req = request, res = response ) => {
    try {
        const { id } = req.params;
        const book = await Book.findByPk( id );
        responseSuccess(res, {message: 'GET_BOOK', response: book});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_GET_BOOK'}, error);
    }
}


/**
 * @name saveBook
 * @description Metodo post encargado de guardar un libro
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const saveBook = async ( req = request, res = response ) => {
    
    const body = req.body;
    try {
        const book = await Book.create( body );
        responseSuccess(res, {message: 'SAVE_BOOK', response: book});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_SAVE_BOOK'}, error);
    }
}


/**
 * @name updateBook
 * @description Metodo encargado de actualizar un libro
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const updateBook = async ( req = request, res = response ) => {
    const { id } = req.params;
    const body = req.body;
    try {
        const book = new Book({...body, id});
        await book.update();
        responseSuccess(res, {message: 'UPDATE_BOOK', response: book});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_UPDATE_BOOK'}, error);
    }
}


/**
 * @name deleteBook
 * @description Metodo encargado de eliminar un libro
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const deleteBook = async( req = request, res = response ) => {
    
    const { id } = req.params;
    try {
        const book = await Book.findByPk( id );

        await book.destroy()
        responseSuccess(res, {message: 'DELETE_BOOK', response: book});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_DELETE_BOOK'}, error);
    }
}


module.exports = {
    getAllBooks,
    getBook,
    saveBook,
    updateBook,
    deleteBook
}