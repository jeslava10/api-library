const { request, response } = require("express");
const bcryptjs = require('bcryptjs');
const {generarJWT} = require('../helpers/jwt.helpers')

const User = require('../models/user.model');
const Role = require("../models/role.model");
const { responseSuccess, responseFailure } = require("./common.controller");


/**
 * @name getAllUsers
 * @description Metodo encargado de obtener todos los usuarios.
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
const login = async ( req = request, res = response ) => {
    
    const { email, password } = req.body;

    try {
        const user = await User.findOne({ where: {
            email
        }, include: Role});

        if ( !user ) {
            return responseFailure(res, {message: 'LOGIN_FAILURE', error: 'USER_NOT_FOUND'});
        }

        console.log('password: ', {password, other: user.password});
        const validatePassword = bcryptjs.compareSync( password, user.password );

        if ( !validatePassword ) {
            return responseFailure(res, {message: 'LOGIN_FAILURE', error: 'PASSWORD_DENIED'});
        }

        const token = await generarJWT( user.id );
        responseSuccess(res, {message: 'LOGIN_SUCCESS', response: { user, token }});
    } catch (error) {
        responseFailure(res, {message: 'LOGIN_FAILURE'}, error);
    }
}

/**
 * @name userRegister
 * @description Metodo encargado de registrar un usuario en el sistma como Usuario
 * @param {Request} req información del request lanzado
 * @param {Response} res información de la respuesta
 */
 const userRegister = async ( req = request, res = response ) => {
    
    const { firstName, secondName = '', lastName, secondLastName = '', email, password } = req.body;
    try {

        const salt = bcryptjs.genSaltSync();
        const pass = bcryptjs.hashSync( password, salt );
        const user = await User.create({
            firstName,
            secondName,
            lastName,
            secondLastName,
            email,
            password: pass,
            roles_id: 2
        }, { include: Role });
        responseSuccess(res, {message: 'REGISTRY_USER', response: user});
    } catch (error) {
        responseFailure(res, {message: 'FAIL_REGISTRY_USER'}, error);
    }
}

module.exports = {
    login,
    userRegister
}