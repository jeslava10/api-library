const { Router } = require('express'); 
const { check } = require('express-validator');

/**
 * @description importar metodos de controlador
 */
const { 
    getAllBooks,
    getBook,
    saveBook,
    updateBook,
    deleteBook
} = require('../controllers/books.controller');

const { isBook } = require('../helpers/database.helper');
const { isAdmin } = require('../middlewares/role.middleware');
const { validarJWT } = require('../middlewares/validar-jwt.middleware');
const { middlewareValidate } = require('../middlewares/validate.middleware');

const router = Router();

router.get('/', [
    validarJWT
], getAllBooks);

/**
 * @name getAllBook
 * @description Ruta hacia el controlador de obtener todos los libros.
 */
router.get('/:id',[
    validarJWT,
    check( 'id', 'NOT PRESENT ID').not().isEmpty().custom( isBook ),
    middlewareValidate
], getBook);


/**
 * @swagger
 *
 * /login:
 *   post:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         in: formData
 *         required: true
 *         type: string
 */
router.post( '/', [
    validarJWT,
    isAdmin
], saveBook );


/**
 * @name updateBook
 * @description Ruta hacia el controlador de modificar un libro.
 */
router.put('/:id',[
    validarJWT,
    isAdmin,
    check( 'id', 'NOT PRESENT ID').not().isEmpty().custom( isBook ),
    middlewareValidate
], updateBook);


/**
 * @name deleteBook
 * @description Ruta hacia el controlador de eliminar un libro.
 */
router.delete('/:id', [
    validarJWT,
    isAdmin,
    check( 'id', 'NOT PRESENT ID').not().isEmpty().custom( isBook ),
    middlewareValidate
], deleteBook);


/**
 * Exportar todas las rutas hacia el respectivo controlador.
 */
module.exports = router;