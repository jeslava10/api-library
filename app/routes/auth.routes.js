const { Router } = require('express'); 
const { check } = require('express-validator');
const { userRegister, login } = require('../controllers/auth.controller');

const { findUserbyEmail, findUserbyEmailError } = require('../helpers/user.helper');
const { middlewareValidate } = require('../middlewares/validate.middleware');


const router = Router();


router.post( '/register', [
    check( 'firstName', 'FIELD_EMPTY' ).not().isEmpty(),
    check( 'lastName', 'FIELD_EMPTY' ).not().isEmpty(),
    check( 'password', 'INVALID_FIELD' ).isLength({ min: 6, max: 12}),
    check( 'email', 'INVALID_FIELD' ).isEmail().custom( findUserbyEmailError ),
    middlewareValidate
], userRegister );


router.post( '/login', [
    check( 'email', 'INVALID_FIELD' ).isEmail().custom( findUserbyEmail ),
    check( 'password', 'INVALID_FIELD' ).isLength({ min: 6, max: 12}),
    middlewareValidate
], login );

/**
 * Exportar todas las rutas hacia el respectivo controlador.
 */
module.exports = router;