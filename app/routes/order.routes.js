const { Router } = require('express');

/**
 * @description importar metodos de controlador
 */
const { 
    getAllOrders,
    getOrderByUser,
    saveOrder,
    updateOrder,
    deleteOrder
} = require('../controllers/order.controller');
const { isAdmin } = require('../middlewares/role.middleware');


const { validarJWT } = require('../middlewares/validar-jwt.middleware');
const { middlewareValidate } = require('../middlewares/validate.middleware');

const router = Router();


router.get('/', [
    validarJWT,
    isAdmin
], getAllOrders);


router.get('/getOrders',[
    validarJWT,
], getOrderByUser);


/**
 * @swagger
 *
 * /login:
 *   post:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         in: formData
 *         required: true
 *         type: string
 */
router.post( '/', [
    validarJWT
], saveOrder );


/**
 * @name deleteBook
 * @description Ruta hacia el controlador de eliminar un libro.
 */
router.delete('/:id', [
    validarJWT
], deleteOrder);



/**
 * Exportar todas las rutas hacia el respectivo controlador.
 */
module.exports = router;